package com.sunny.study.account.email;

import static org.junit.Assert.assertEquals;

import javax.mail.Message;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetup;

public class AccountEmailServiceImplTest {

	private GreenMail greenMail;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("AccountEmailServiceImplTest.setUpBeforeClass()");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("AccountEmailServiceImplTest.tearDownAfterClass()");
	}

	@Before
	public void startMailServer() throws Exception {
		greenMail = new GreenMail(ServerSetup.SMTP);
		greenMail.setUser("sunshineboy919@gmail.com", "bean1314love+");
		greenMail.start();
	}

	@After
	public void tearDown() throws Exception {
		greenMail.stop();
	}

	@Test
	public void testSendMail() throws Exception {
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"account-email.xml");
		AccountEmailService accountEmailService = (AccountEmailService) ctx
				.getBean("accountEmailService");

		String subject = "maven study";
		String htmlText = "<h3>maven study begin</h3>";
		String to = "649663915@qq.com";
		accountEmailService.sendMail(to, subject, htmlText);

		greenMail.waitForIncomingEmail(2000, 1);
		Message[] msgs = greenMail.getReceivedMessages();
		assertEquals(1, msgs.length);
		assertEquals(subject, msgs[0].getSubject());
		assertEquals(htmlText, GreenMailUtil.getBody(msgs[0]).trim());

	}

}
