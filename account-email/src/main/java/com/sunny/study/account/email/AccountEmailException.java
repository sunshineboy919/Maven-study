package com.sunny.study.account.email;

public class AccountEmailException extends Exception {
	private static final long serialVersionUID = 1L;

	public AccountEmailException(String message, Throwable cause) {
		super(message, cause);
	}

	public AccountEmailException(String message) {
		super(message);
	}

}
